# Name
Graphic Herbalism

# Created by
Stuporstar
Greatness7

# Category
Immersion

# Description
Now works with OpenMW! MWSE Graphic Herbalism harnesses the power of lua and mesh switchnodes, allowing this mod to be run without an esp or standard scripts. This dramatically optimizes performance compared to older graphic herbalism mods. It also comes with an mod configuration menu to allow you to change settings.

# Tags
* Performance Optimization
* Gameplay Effects/Changes
* Replacer
* Plants / Foliage
* OpenMW
* May Modathon 2019

# Release date
2019-05-01 06:32

# Last update date
2019-05-23 21:35

# Version
1.04

# Changelog
## Version 1.04
* Fixed pixelation on mucksponge and cavernspore decals.
* Fixed pixelation on Apel's replacer mucksponges and all glowmaps in replacers file.
* Fixes pixelation on picked textures in some TR meshes.

## Version 1.03
* Fix for MWSE bug that makes organic containers disappear on respawn until cell is reloaded.
* Fixed wrong MCM labels on whitelist.
* Updated instructions in old GH save cleaner readme.

## Version 1.02
* Version 1.02 Warning message for out of date MWSE version fixed.
* Added warning message for out of date Quickloot.
* Added esp patch for missing organic flag on tramaroot_06 as a separate download.

## Version 1.01
* Version 1.01 Removed collision from chokeweed and roobrush because it's annoying.
* Fixed marshmerrow_03 - both vanilla and smoothed version.
* Fixed kwama egg sac.
* Made picked kreshweed more obvious, cutting all their leaves instead of a few.
* Uprooted corkbulb instead of making it disappear.
* Removed more leaves from Apel's picked kreshweed.
* Removed collision from Vurt's chokeweed and roobrush and removed some leaves from the chokeweed.
* Updated MWSE scripts

# Credits
* Greatness7 invented all the scripts for this mod
* Nullcascade for his continued work on MWSE who worked closely with us on this mod
* Petethegoat - script help and testing, and also trimmed the chokeweed and roobrush meshes
* Sveng - feedback and playtesting
* Merlord - MCM
* Remiros - MOP meshes (used as base for half the vanilla meshes)
* Stuporstar - main mesh adaptor, as well as any smoothed meshes textures not listed below
* Manauser and Scrawafunda - picked textures for comberry, holly, and lloramor
* GrunTella - picked texture for heather, smoothed trama root, smoothed stoneflower
* Moranar - many of the smoothed meshes were adapted from Better Flora
* Tyddy - smoothed chokeweed and roobrush (with further smoothing and UV adjustments by me)
* DassiD - alpha maps for picked moss (used in ST Alchemy patch) are were made from Morrowind Enhanced Textures
* Articus - wickwheat leaves included in mesh replacer package

* Thanks for Manauser and Skrawafunda for their work on the original Graphic Herbalism
* Thanks to Nich and CJW-Craigor for original UV Correct Ores and Ore Diversity

# Permissions
You may adapt and use any part of this mod so long as the original modders are credited.
Please see individual credits for who did what.

## Distribution
### Allowed
* All the assets in this file belong to the author, or are from free-to-use modder's resources
* You can upload this file to other sites but you must credit me as the creator of the file
* You are allowed to modify my files and release bug fixes or improve on the features so long as you credit me as the original creator
* You are allowed to use the assets in this file without permission as long as you credit me
* You are allowed to earn Donation Points for your mods if they use my assets

### Disallowed
* You are not allowed to convert this file to work on other games under any circumstances
* You are not allowed to use assets from this file in any mods/files that are being sold, for money, on Steam Workshop or other platforms
