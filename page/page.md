# --> ![Features](features.png) <--

## --> It's finally time to ditch the scripts with the new Graphic Herbalism! <--

The old scripted containers were one of the largest CPU sinks you could add to your game. Without the scripts, this new GH is optimal. All the old meshes have been optimized as well.

#### --> These meshes now work with OpenMW's native graphic herbalism, now available in their latest nightly! <--

Automatically harvests herbs just like the old Graphic Herbalism, but using the container's leveled lists. This means
it's compatible with any mod that alters the contents or containers
leveled lists, without patches.

*MWSE Only Features:*
The MWSE script adds tooltips so you can see what ingredients you're picking. It even hides effects depending on your alchemy skill.

*Has MCM configuration menu, so you can tweak settings easily in-game. Current features include:*
* Turn tooltips on and off - if you're running another mod, like Quickloot, turning GH tooltips off will default to whichever mod handles tooltips next in the list.
* Turn message boxes on or off.
* Adjust volume of harvesting sound.
* Blacklist and Whitelist tabs - pulls from all currently loaded mods to seach for organic containers.

## --> ![FAQ](faq.png) <--

#### Q. Is it compatible with Morrowind Rebirth?
A.    Yes. Install the GH meshes AFTER Rebirth.

#### Q. Is it compatible with OpenMW?
A.    Yes! Native graphic herbalism is now available in OpenMW's latest nightly build.. You only need the meshes, not the included MWSE scripts.

#### Q. Is it compatible with other Herbalism mods or older Graphic Herbalism patches?
A.    No, it's not compatible with scripted plants, so uninstall all old GH patches and add-ons. A Wrye Mash remover is available as a separate download so you can clean your saves of old GH content.

#### Q. What about Graphic Herbalism Extra?
A.    An MWSE version is being planned for a future update. The old GH Extra isn't compatible with this mod.

#### Q. Can I use the old Graphic Herbalism meshes with this mod?
A.    No, the old meshes don't have the required switchnodes.

#### Q. Is it compatible with Pearls Enhanced?
A.     YES! More than the old GH ever was. No patches necessary.

#### Q. Is it compatible with Happy Harvesting?
A.    Happy Harvesting is included in this mod, so it's no longer necessary, but it won't break anything.

#### Q. Is it compatible with Quickloot?
A. If you're talking about mort's lua Quickloot, yes the latest version is compatible. Older versions of Quickloot cause problems.

#### Q. Is it compatible with Immersive Mining?
A.    No, but eventually we're going to roll that into a new gameplay harvesting mod that's designed to work with this one.

#### Q. Is is compatible with Diverse UV Correct Ore?
A.    Yes, UV Correct Ore was used as the base for this mod. A patched version of the Diverse UV Correct Ore esp is included in the Patches and Replacers archive. Use it instead of the original mod.

#### Q. Is is compatible with Morrowind Optimization Patch?
A.    MOP was used as the base for this mod. Load the vanilla GH meshes after installing MOP.

#### Q. Are there Epic Plants versions?
A.    I gave Articus a huge package of converted meshes, which he'll be hosting and maintaining on the Epic Plants download page.
    The smoothed mesh folder contains some meshes already used in Epic Plants, and his textures are compatible with all of them (and Pherim's fire fern). Consider these slightly less high poly alternatives to Epic's if you're performance is lagging.

#### Q. Can it be used with texture replacers for the old Graphic Herbalism?
A.    Yes, they have the same file names as the old GH textures. If you know of any decent ones, let me know and I'll link them.

#### Q. Is it compatible with Animated Containers?
A.    AC will override kollop behavior because GH ignores scripted containers. If they're whitelisted, they'll function like GH containers instead.

#### Q. Is it compatible with Expanded Sounds?
A.    Flora with ES scripts have been whitelisted because they don't use the OnActivate function, so it's compatible.

### Troubleshooting

#### Q. I get an error message saying MWSE is out of date when I load Morrowind.
A.    Run the MWSE-Update.exe in your main Morrowind folder. If it's not there, reinstall it from MWSE 2.1 dev: https://nullcascade.com/mwse/mwse-dev.zip

#### Q. I'm missing textures.
A.    1. Make sure the base Graphic Herbalism textures are installed.
     2. Make sure the optional meshes you chose don't require textures from mods you didn't install.

#### Q. The container acts like a regular container, not a GH container.
    1. Try whitelisting the container. If whitelisting makes it function, that means it was scripted by another mod.
    2. Be aware that whitelisting containers can break mods if they use the OnActivate function.
    3. If it doesn't show up on the whitelist, that means the container is not flagged as organic.
    4. Check your mods for that container reference, and if it's not organic like it should be, you can check it off in the CS and resave the mod - or ask the modder to fix it.

#### Q. I can harvest the container but there's no change graphically. The plants don't disappear.
A.    1. Check your meshes folders to make sure the container mesh contains the switch node HerbalismSwitch.
    2. Reinstall the GH mesh. If you're using a replacer, patches for most replacers are in the separate GH Patches and Replacers download. GH meshes must be installed after any others.

#### Q. The tooltip is not behaving as expected.
A.    Another lua mod could be interfering with GH's behavior. Check for any lua mods that deal with tooltips or containers, update them if necessary, and otherwise report any conflicts to us.

## --> ![Installation](installation.png) <--

#### OpenMW Users - make sure you have the latest nightly build: https://forum.openmw.org/viewtopic.php?t=1808
You can completely ignore the MWSE folder - only the meshes are required.
You don't need to enable anything in OpenMW either, just make sure the GH meshes are loaded with the highest priority.

#### MWSE Users - first make sure you have installed the following:
MGEXE - https://www.nexusmods.com/morrowind/mods/41102
MWSE 2.1 Development version - https://nullcascade.com/mwse/mwse-dev.zip - make sure to use the included updater!
EasyMCM is no longer required as it's now included in MWSE itself.

1. Install the core mod and then install the smoothed meshes if you prefer. Optional meshes for replacers are included in separate downloads.
2. Install GH meshes AFTER you've installed all your mods that include flora and ore replacers. This includes Morrowind Rebirth!
3. If you get an error message to update MWSE, use the MWSE-Update.exe. Same goes for MCM in the Mod Config menu.
4. This mod will run without Easy MCM, but you won't be able to configure it in the mod config menu - the extra trouble is worth it.

The optional package includes meshes from many popular replacers, packaged so you can pick and choose in any installer. Most of these are patches, so they require the original mods installed first. See included readmes for extra help and suggested load orders.

# --> ![Credits](credits.png) <--

* Greatness7 invented all the scripts for this mod
* Nullcascade for his continued work on MWSE who worked closely with us on this mod
* Petethegoat - script help and testing, and also trimmed the chokeweed and roobrush meshes
* Sveng - feedback and playtesting
* Merlord - MCM
* Remiros - MOP meshes (used as base for half the vanilla meshes)
* Stuporstar - main mesh adaptor, as well as any smoothed meshes textures not listed below
* Manauser and Scrawafunda - picked textures for comberry, holly, and lloramor
* GrunTella - picked texture for heather, smoothed trama root, smoothed stoneflower
* Moranar - many of the smoothed meshes were adapted from Better Flora
* Tyddy - smoothed chokeweed and roobrush (with further smoothing and UV adjustments by me)
* DassiD - alpha maps for picked moss (used in ST Alchemy patch) are were made from Morrowind Enhanced Textures
* Articus - wickwheat leaves included in mesh replacer package

--> Thanks for Manauser and Skrawafunda for their work on the original Graphic Herbalism <--
--> Thanks to Nich and CJW-Craigor for original UV Correct Ores and Ore Diversity <--

# --> ![Part of Morrowind May Modathon 2019](footer.png) <--
